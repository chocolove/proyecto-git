﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
	Vector3 target;

	public GameObject bulletPrefab;
	public GameObject bulletSpawnpoint;
    private Transform myTransform;
	public float speed;
	private Rigidbody rb;
	
	private Vector3 moveInput;
	private Vector3 moveVelocity;

	private Camera mainCamera;

	public GunController theGun;

    // Start is called before the first frame update
    void Start() {
		myTransform = GetComponent<Transform>();
 		rb = GetComponent<Rigidbody>();

		mainCamera = FindObjectOfType<Camera>();
		target = transform.position;

	}
	
	// Update is called once per frame
	void Update () {
		Movement();
		shoot1();

	}
	void Movement()
	{
		Ray cameraRay = mainCamera.ScreenPointToRay(Input.mousePosition);
		Plane groundPlane = new Plane(Vector3.up, Vector3.zero);

		float rayLength;
		
		if(groundPlane.Raycast(cameraRay, out rayLength))
		{
			Vector3 pointToLook = cameraRay.GetPoint(rayLength);
			Debug.DrawLine(cameraRay.origin, pointToLook, Color.blue);

			transform.LookAt(new Vector3(pointToLook.x, transform.position.y, pointToLook.z));
		}

		if (Input.GetKey (KeyCode.W)) 		{
			myTransform.Translate (new Vector3 (0, 0, speed) * Time.deltaTime);
		}

		if (Input.GetKey (KeyCode.S)) 		{
			myTransform.Translate (new Vector3 (0, 0, -speed) * Time.deltaTime);
		}

		if (Input.GetKey (KeyCode.A)) 		{
			myTransform.Translate (new Vector3 (-speed, 0, 0) * Time.deltaTime);
		}

		if (Input.GetKey (KeyCode.D)) 		{
			myTransform.Translate (new Vector3 (speed, 0, 0) * Time.deltaTime);
		}
	}
	void shoot1()
	{
		if (Input.GetMouseButtonDown(0))
		{
			theGun.isFiring = true;
		}
		if (Input.GetMouseButtonUp(0))
		{
			theGun.isFiring = false;
		}
		
	}
	void OnCollisionEnter(Collision other)
	{

		if(other.gameObject.tag=="Enemy")
		{
			SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 0);
			//SceneManager.LoadScene("Scena3");
		}

	}
}
