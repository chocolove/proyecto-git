﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crear : MonoBehaviour {
	public float timer;
	public float shootingDelay;
    public static float healthAmount;
	public GameObject bulletPrefab;
	public GameObject bulletSpawnpoint;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		shootDelay2();
	}
	void shootDelay2()
	{
		timer+=Time.deltaTime;
		if(timer>=shootingDelay)
		{
			GameObject temporalBullet=Instantiate(bulletPrefab);
			temporalBullet.transform.position=bulletSpawnpoint.transform.position;
			timer=0;

		}

	}
}
