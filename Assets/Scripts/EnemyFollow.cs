﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyFollow : MonoBehaviour
{
    public float lookRadius = 10f;
    public Transform target;
    public Transform myTransform;
    
    void Start()
    {

    }
    // Update is called once per frame
    void Update()
    {
        transform.LookAt(target);
        transform.Translate(Vector3.forward*5*Time.deltaTime);
    }
    void OnDrawGizmosSelected ()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, lookRadius);
    }
	void OnCollisionEnter(Collision other)
	{
		if(other.gameObject.tag=="Bullet1")
		{
          	Destroy(gameObject);
			Destroy(other.gameObject);
		}
	}
	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject.tag=="Bullet1")
		{
          	Destroy(gameObject);
			Destroy(other.gameObject);
		}
    }
}
